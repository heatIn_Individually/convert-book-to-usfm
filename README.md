# Convert Book to USFM

Script that converts Scriptural book text to USFM

## Name
Convert book to USFM

## Description
This project is an attempt to create a script that will take chunks of inputted Scripture text basis edition and convert them to [USFM files](https://paratext.org/usfm/) more efficiently. USFM files are a text format used in Bible translation software, such as [Bibledit](https://bibledit.org/) or [Paratext](https://paratext.org/). The program takes input from a book.txt file and outputs to a file called bookConvert.usfm.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
To use this script, use Python to run it within its file directory with a book.txt file of input text under the same directory. It will output to a file called bookConvert.usfm.
The input text should be in a format with chapter numbers, then verses separated by verse numbers. The book.txt file in the repo is provided as an example. The bookConvert.usfm file is provided as an example of what the output will look like.
The output file will still need book identifier codes added, such as [\id](https://ubsicap.github.io/usfm/identification/index.html#index-1), [\h](https://ubsicap.github.io/usfm/identification/index.html#h), and [\toc2](https://ubsicap.github.io/usfm/identification/index.html#usfmp-toc).

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Thank you to all of the contributors to this project! May the Lord God remember you in his kingdom always, now and ever, and unto ages of ages, amen.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
