## Outsourced version, read full book
#read book into list of lines
book = [ ]
with open("book.txt", 'r', encoding='utf8') as f:
    book = [line.strip() for line in f.readlines() if line.strip()]
chap = 0

#begin writing to converted book
with open("bookConvert.usfm", 'w', encoding='utf8') as f:
    for line in book:
        #based on line length, chapter number is never more than 10 chars long
        if len(line) > 10:
            #write chapter start and paragraph start
            f.write('\\c ' + str(chap) + '\n' + '\\p' + '\n')
            #repeat while not out of verses
            vCount = 1
            while vCount > 0:
                #find first verse position
                pos1 = line.find(str(vCount))
                #int to track how many omitted verses there are
                omitted = 0
                #find second verse poition, else position at end if next 3 verses dont exist
                checks = 5 #number of omission checks. Can be increased as needed.
                pos2 = line.find(str(vCount + 1))
                while checks > 0:
                    if (pos2 == -1):
                        vCount = vCount + 1
                        pos2 = line.find(str(vCount + 1))
                        omitted = omitted + 1
                    else:
                        break
                    checks = checks - 1
                if pos2 == -1:
                    pos2 = len(line)
                    omitted = 0
                #if both loacations identified continue
                if not (pos1 == -1 or pos2 == -1):
                    #substr the verse out of the line
                    verse = line[pos1:pos2]
                    #write verse to book
                    f.write('\\v' + ' ' + verse.strip() + '\n')
                    #write omitted verse markers
                    if omitted > 0:
                        while omitted > 0:
                            f.write('\\v ' + str(vCount - (omitted-1)) + '\n')
                            omitted = omitted - 1
                    #increment verse count
                    vCount = vCount + 1
                else:
                    #otherwise set to -1
                    vCount = -1
                
        else:
            chap = chap + 1
